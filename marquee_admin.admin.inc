<?php

/**
 * @file
 * Adminstration page callbacks for the Marquee Admin module
 */
 
/**
 * Form builder. Configure Marquee Admin
 *
 */
 
function marquee_admin_admin_settings() {
	$form['marquee_admin_view_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Name of the view used for the marquee'),
		'#default_value' => variable_get('marquee_admin_view_name', array('page')),
		'#description' => t('Enter the name of the view you want to add administration links to'),
	);
	$form['marquee_admin_view_display'] = array(
		'#type' => 'textfield',
		'#title' => t('Name of the display block <i>display</i> used for the marquee'),
		'#default_value' => variable_get('marquee_admin_display_name', array('put display name here')),
		'#description' => t('Enter the name of the view <i>disaply</i> you want to add administration links to'),
	);
	$form['marquee_admin_drag_display'] = array(
		'#type' => 'textfield',
		'#title' => t('Name of the draggable view <i>display</i> used for the marquee'),
		'#default_value' => variable_get('marquee_admin_drag_name', array('put draggable display name here')),
		'#description' => t('Enter the name of the view <i>disaply</i> you want to add administration links to'),
	);
	$form['marquee_admin_node_type'] = array(
		'#type' => 'textfield',		'#title' => t('Machine name of the node type used for the marquee'),
		'#default_value' => variable_get('marquee_admin_node_type', array('put node type here')),
		'#description' => t('Enter the name of the node type used in the marquee'),
	);
	$form['marquee_admin_number'] = array(
		'#type' => 'textfield',		'#title' => t('Number of slides in your marquee'),
		'#default_value' => variable_get('marquee_admin_number', array('')),
		'#description' => t('Enter the number of slides used in the marquee'),
	);
	$form['#submit'][] = 'marquee_admin_admin_settings_submit';
	return system_settings_form($form);

}
function marquee_admin_admin_settings_submit($form, $form_state) {
	variable_set('marquee_admin_view_name', $form_state['values']['marquee_admin_view_name']);
	variable_set('marquee_admin_display_name', $form_state['values']['marquee_admin_view_display']);
	variable_set('marquee_admin_drag_name', $form_state['values']['marquee_admin_drag_display']);
	variable_set('marquee_admin_node_type', $form_state['values']['marquee_admin_node_type']);
	variable_set('marquee_admin_number', $form_state['values']['marquee_admin_number']);
}