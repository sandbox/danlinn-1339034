(function($) {

Drupal.behaviors.metaltoad = {
  attach: function (context) {  
  console.log(Drupal.settings.marquee_admin.view_name);
  console.log(Drupal.settings.marquee_admin.marquee_num);
  $('#div-line').remove();
  //Modify the look of the draggable views
  $newrow = document.createElement("tr");
  $newcell = document.createElement("td");
  $($newrow).css("border-top","3px dashed black").addClass("draggable").attr("id","div-line").css("width","100%");
  $($newcell).text("Only slides above this line will be shown").attr("colspan","2");
  $($newrow).append($newcell);
		$('.view-id-'+Drupal.settings.marquee_admin.view_name+' .tabledrag-processed tr:nth-child('+Drupal.settings.marquee_admin.marquee_num+')').after($newrow);
		$('.view-id-'+Drupal.settings.marquee_admin.view_name+' .tabledrag-processed tr').css("opacity",".5");
		$('.view-id-'+Drupal.settings.marquee_admin.view_name+' .tabledrag-processed tr:nth-child(-n'+Drupal.settings.marquee_admin.marquee_num+')').css("opacity","1");
   }
    
  };

})(jQuery);